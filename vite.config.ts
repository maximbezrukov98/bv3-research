import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

const config = defineConfig({
  test: {
    environment: 'jsdom',
  },
  plugins: [vue()],
  optimizeDeps: {
    exclude: ['vue'],
  },
})

export default config
