import { ComponentInternalInstance, ComponentPublicInstance } from 'vue'

export const findContextBySymbolName = <T>(
  instance: ComponentInternalInstance,
  name: string
): T | null => {
  const provides = (instance as unknown as { provides: Record<symbol, unknown> }).provides
  const matchingSymbol = Object.getOwnPropertySymbols(provides).find(
    (s) => s.toString() === `Symbol(${name})`
  )
  return matchingSymbol ? (provides[matchingSymbol] as T) : null
}
