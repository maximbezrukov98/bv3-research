import { createApp } from 'vue'
import 'bootstrap/dist/css/bootstrap.css'
import Root from './components/root.vue'
import BAlert from '../src/components/alert/Alert.vue'

const app = createApp(Root)
app.component('BAlert', BAlert)
app.mount('#app')
