import { computed, Ref } from 'vue'

export const useComputedBooleanAttribute = (prop: Ref<string | boolean>) =>
  computed(() => {
    return prop.value === '' ? true : prop.value
  })
