import { inject } from 'vue'
import { defaultEventHub, EVENT_HUB_SYMBOL } from '../eventHub/eventHub'

// TODO: improve types
export function useEventHub() {
  const eventHub = inject(EVENT_HUB_SYMBOL, defaultEventHub)

  return eventHub
}
