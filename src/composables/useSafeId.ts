// SSR safe client-side ID attribute generation
// ID's can only be generated client-side, after mount
// `this._uid` is not synched between server and client

import { ref, computed, ExtractPropTypes, onMounted } from 'vue'

export const props = {
  id: { type: String },
}

let globalIdCounter = 1

export const useSafeId = (cmpProps: ExtractPropTypes<typeof props>) => {
  const localId = ref(null as null | string)
  const safeId = computed(() => (suffix = '') => {
    const id = cmpProps.id || localId.value
    if (!id) {
      return null
    }
    suffix = suffix.replace(/\s+/g, '_')
    return suffix ? `${id}_${suffix}` : id
  })

  onMounted(() => {
    localId.value = `__BVID__${globalIdCounter}`
    globalIdCounter += 1
  })

  return { safeId }
}
