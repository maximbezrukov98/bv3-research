import { RX_NUMBER } from '../constants/regex'

// --- Convenience inspection utilities ---

export const toType = (value: unknown) => typeof value

export const toRawType = (value: unknown): string =>
  Object.prototype.toString.call(value).slice(8, -1)

export const toRawTypeLC = (value: unknown): string => toRawType(value).toLowerCase()

export const isNumeric = (value: unknown): value is number => RX_NUMBER.test(String(value))

export const isPrimitive = (value: unknown): value is boolean | string | number =>
  ['boolean', 'string', 'number'].includes(toType(value))

// Quick object check
// This is primarily used to tell Objects from primitive values
// when we know the value is a JSON-compliant type
// Note object could be a complex type like array, Date, etc.
export const isObject = (obj: unknown) => obj !== null && typeof obj === 'object'

// Strict object type check
// Only returns true for plain JavaScript objects
export const isPlainObject = (obj: unknown): obj is Record<string, unknown> =>
  Object.prototype.toString.call(obj) === '[object Object]'

export const isRegExp = (value: unknown): value is RegExp => toRawType(value) === 'RegExp'

export const isPromise = (value: unknown) => typeof value?.then === 'function'
