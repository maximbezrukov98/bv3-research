import { RX_ENCODED_COMMA, RX_ENCODE_REVERSE } from '../constants/regex'
import { isPlainObject } from './inspect'
import { toString } from './string'

// Method to replace reserved chars
const encodeReserveReplacer = (c: string) => '%' + c.charCodeAt(0).toString(16)

// Fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
const encode = (str: unknown) =>
  encodeURIComponent(toString(str))
    .replace(RX_ENCODE_REVERSE, encodeReserveReplacer)
    .replace(RX_ENCODED_COMMA, ',')

// const decode = decodeURIComponent

// Stringifies an object of query parameters
// See: https://github.com/vuejs/vue-router/blob/dev/src/util/query.js
export const stringifyQueryObj = (
  obj?: Record<string, undefined | null | string | Array<undefined | null | string>>
) => {
  if (!isPlainObject(obj)) {
    return ''
  }

  const query = Object.keys(obj)
    .map((key) => {
      const value = obj[key]
      if (value === undefined) {
        return ''
      } else if (value === null) {
        return encode(key)
      } else if (Array.isArray(value)) {
        return value
          .reduce((results, value2) => {
            if (value2 === null) {
              results.push(encode(key))
            } else if (value2 !== undefined) {
              // Faster than string interpolation
              results.push(encode(key) + '=' + encode(value2))
            }
            return results
          }, [] as string[])
          .join('&')
      }
      // Faster than string interpolation
      return encode(key) + '=' + encode(value)
    })
    /* must check for length, as we only want to filter empty strings, not things that look falsey! */
    .filter((x) => x.length > 0)
    .join('&')

  return query ? `?${query}` : ''
}

// export const parseQuery = (query) => {
//   const parsed = {}
//   query = toString(query).trim().replace(RX_QUERY_START, '')

//   if (!query) {
//     return parsed
//   }

//   query.split('&').forEach((param) => {
//     const parts = param.replace(RX_PLUS, ' ').split('=')
//     const key = decode(parts.shift())
//     const value = parts.length > 0 ? decode(parts.join('=')) : null

//     if (isUndefined(parsed[key])) {
//       parsed[key] = value
//     } else if (isArray(parsed[key])) {
//       parsed[key].push(value)
//     } else {
//       parsed[key] = [parsed[key], value]
//     }
//   })

//   return parsed
// }

export const isLink = (props: { href?: unknown; to?: unknown }) => Boolean(props.href || props.to)
