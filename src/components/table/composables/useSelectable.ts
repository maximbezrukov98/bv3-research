import { provide, inject, computed, ComputedRef } from 'vue'
import { VueCSSClassValue } from '../../../../types'

const TABLE_SELECTABLE_CONTEXT_SYMBOL = Symbol('bv-table-selectable-context')

interface SelectableContext {
  supportsSelectableRows: ComputedRef<boolean>
  isRowSelected: (idx: number) => boolean
  selectAllRows: () => void
  clearSelected: () => void
  selectRow: (idx: number) => void
  unselectRow: (idx: number) => void
  selectableRowClasses: (idx: number) => VueCSSClassValue
  selectableRowAttrs: (idx: number) => Record<string, number>
  isSelectable: ComputedRef<boolean>
  hasSelectableRowClick: ComputedRef<boolean>
}

export const useSelectable = (ctx: SelectableContext) => {
  provide(TABLE_SELECTABLE_CONTEXT_SYMBOL, ctx)
}

const noop = () => {
  // intentionally empty
}

export const useSelectableContext = () =>
  inject<SelectableContext>(TABLE_SELECTABLE_CONTEXT_SYMBOL, {
    supportsSelectableRows: computed(() => false),
    isSelectable: computed(() => false),
    hasSelectableRowClick: computed(() => false),
    selectRow: noop,
    unselectRow: noop,
    isRowSelected: () => false,
    selectAllRows: noop,
    clearSelected: noop,
    selectableRowClasses: () => ({}),
    selectableRowAttrs: () => ({}),
  })
