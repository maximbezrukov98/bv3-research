import { ComputedRef, provide, computed, inject } from 'vue'
import { NormalizedField } from '../utils/normalizeFields'

const TABLE_SORTABLE_CONTEXT_SYMBOL = Symbol('bv-table-sortable-context')

interface SortableContext {
  isSortable: ComputedRef<boolean>
  sortTheadThAttrs: (
    key: string,
    field: NormalizedField,
    isFoot: boolean
  ) => Record<string, unknown>
  sortTheadThClasses: (
    key: string,
    field: NormalizedField,
    isFoot: boolean
  ) => Record<string, unknown>
  sortTheadThLabel: (key: string, field: NormalizedField, isFoot: boolean) => string | null
}

export const useSortable = (ctx: SortableContext) => {
  provide(TABLE_SORTABLE_CONTEXT_SYMBOL, ctx)
}

export const useSortableContext = () =>
  inject<SortableContext>(TABLE_SORTABLE_CONTEXT_SYMBOL, {
    isSortable: computed(() => false),

    sortTheadThAttrs: () => ({}),
    sortTheadThClasses: () => ({}),
    sortTheadThLabel: () => null,
  })
