import { computed, ComputedRef, provide, inject } from 'vue'

const TABLE_STACKED_CONTEXT = Symbol('bv-table-stacked-context')

interface StackedContext {
  isStacked: ComputedRef<string | boolean>
  isStackedAlways: ComputedRef<boolean>
  stackedTableClasses: ComputedRef<Record<string, unknown>>
}

export const useStacked = ({
  isStacked,
}: {
  isStacked: ComputedRef<string | boolean>
}): StackedContext => {
  const isStackedAlways = computed(() => isStacked.value === true)
  const stackedTableClasses = computed(() => ({
    'b-table-stacked': isStackedAlways.value,
    [`b-table-stacked-${isStacked.value}`]: typeof isStacked.value === 'string',
  }))

  const context = {
    isStacked,
    isStackedAlways,
    stackedTableClasses,
  }
  provide(TABLE_STACKED_CONTEXT, context)

  return context
}

export const useStackedContext = () => {
  const context = inject<StackedContext>(TABLE_STACKED_CONTEXT)

  if (!context) {
    throw new Error('useStackedContext must be used within a Table component')
  }

  return context
}
