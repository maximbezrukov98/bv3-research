// import { EVENT_NAME_CONTEXT_CHANGED } from '../../../constants/events'
import { PropType, Ref, ExtractPropTypes, computed, provide, inject, ComputedRef } from 'vue'
import { FieldValue, NormalizedField, FormatterFn, normalizeFields } from '../utils/normalizeFields'
import { lookupValueFromParent } from './useLookupValueFromParent'

const TABLE_FIELDS_CONTEXT_SYMBOL = Symbol('bv-table-fields-context')

export const props = {
  // modelValue: { type: Array as PropType<unknown[] | null>, default: () => [] },
  // LEGACY: use `modelValue` instead
  // value: { type: Array as PropType<unknown[] | null>, default: () => [] },
  fields: { type: Array as PropType<FieldValue[] | null>, default: () => [] },
  items: { type: Array as PropType<Record<string, unknown>[] | null>, default: () => [] },
  // primaryKey: { type: String },
}

type TableState = {
  items: Ref<Record<string, unknown>[]>
}

type NormalizedFieldWithFormatter = Omit<NormalizedField, 'formatter'> & {
  formatter: undefined | FormatterFn
}

type FieldsContext = {
  fields: ComputedRef<NormalizedField[]>
  getFormattedValue: (item: Record<string, unknown>, field: NormalizedField) => unknown
}

export const useFields = (
  cmpProps: ExtractPropTypes<typeof props>,
  { items }: TableState
): FieldsContext => {
  const computedFields = computed(() => normalizeFields(cmpProps.fields, items.value))
  const computedFieldsObj = computed(() =>
    Object.fromEntries(
      computedFields.value.map((f) => [
        f.key,
        {
          ...f,
          ...(f.formatter
            ? {
                formatter: lookupValueFromParent(f.formatter),
              }
            : {}),
        } as NormalizedFieldWithFormatter,
      ])
    )
  )

  const getFieldFormatter = (key: string) => computedFieldsObj.value[key]?.formatter
  const getFormattedValue = (item: Record<string, unknown>, field: NormalizedField) => {
    const { key } = field
    const formatter = getFieldFormatter(key)
    const rawValue = item[key] ?? null
    const value = formatter?.(rawValue, key, item, field) ?? rawValue

    return value == null ? '' : value
  }

  const context: FieldsContext = {
    fields: computedFields,
    // computedFieldsObj,
    getFormattedValue,
  }

  provide(TABLE_FIELDS_CONTEXT_SYMBOL, context)

  return context
}

export const useFieldsContext = (): FieldsContext => {
  const context = inject<FieldsContext>(TABLE_FIELDS_CONTEXT_SYMBOL)
  if (!context) {
    throw new Error('useFieldsContext must be used within a Table component')
  }
  return context
}

// context() {
//   // Current state of sorting, filtering and pagination props/values
//   return {
//     filter: this.localFilter,
//     sortBy: this.localSortBy,
//     sortDesc: this.localSortDesc,
//     perPage: mathMax(toInteger(this.perPage, 0), 0),
//     currentPage: mathMax(toInteger(this.currentPage, 0), 1),
//     apiUrl: this.apiUrl,
//   }
//   watch: {

// items(newValue) {
//   // Set `localItems`/`filteredItems` to a copy of the provided array
//   this.localItems = isArray(newValue) ? newValue.slice() : []
// },
// // Watch for changes on `computedItems` and update the `v-model`
// computedItems(newValue, oldValue) {
//   if (!looseEqual(newValue, oldValue)) {
//     this.$emit(MODEL_EVENT_NAME, newValue)
//   }
// },
// Watch for context changes
// context(newValue, oldValue) {
//   // Emit context information for external paging/filtering/sorting handling
//   if (!looseEqual(newValue, oldValue)) {
//     this.$emit(EVENT_NAME_CONTEXT_CHANGED, newValue)
//   }
// },
// },
// mounted() {
// Initially update the `v-model` of displayed items
// this.$emit(MODEL_EVENT_NAME, this.computedItems)
// },
// methods: {
// Method to get the formatter method for a given field key
// },
// })
