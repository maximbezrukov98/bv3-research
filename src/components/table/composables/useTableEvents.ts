import { PropType, provide, inject, computed, ExtractPropTypes, ComputedRef } from 'vue'
import { NormalizedField } from '../utils/normalizeFields'

const TABLE_EVENTS_SYMBOL = Symbol('bv-table-events')

export const props = {
  onHeadClicked: { type: Function as PropType<OnHeadClickedHandler> },
  onRowClicked: { type: Function as PropType<OnRowClickedHandler> },
}

type OnRowClickedHandler = (item: Record<string, unknown>, index: number, event: MouseEvent) => void

type OnHeadClickedHandler = (
  key?: string,
  field?: NormalizedField,
  event?: MouseEvent,
  isFoot?: boolean
) => void

type TableEventsEmit = {
  (e: 'head-clicked', ...rest: Parameters<OnHeadClickedHandler>): void
}

type TableEventsContext = {
  emit: TableEventsEmit
  handlers: ComputedRef<ExtractPropTypes<typeof props>>
}

export const provideTableEventsContext = (
  eventHandlers: ExtractPropTypes<typeof props>,
  emit: TableEventsEmit
) => {
  const context: TableEventsContext = {
    emit,
    handlers: computed(() =>
      Object.fromEntries(
        Object.keys(eventHandlers)
          .filter((k): k is keyof typeof props => Object.keys(props).includes(k))
          .map((k) => [k, eventHandlers[k]])
      )
    ),
  }
  provide(TABLE_EVENTS_SYMBOL, context)
}

export const useTableEventsContext = (): TableEventsContext => {
  const context = inject(TABLE_EVENTS_SYMBOL)

  if (!context) {
    throw new Error('useTableEventsContext must be used within a Table component')
  }

  return context as TableEventsContext
}
