import { getCurrentInstance, ComponentInternalInstance, provide, inject } from 'vue'

const INSTANCE_FOR_PARENT_LOOKUP_CONTEXT = Symbol('bv-instance-for-parent-lookup')

export const registerInstanceForParentLookup = () => {
  const instance = getCurrentInstance()
  if (!instance) {
    throw new Error('useRegisterInstanceForParentLookup must be used within a component')
  }

  provide(INSTANCE_FOR_PARENT_LOOKUP_CONTEXT, instance)
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type LookupFn<T> = T extends (...args: any[]) => any
  ? (...args: Parameters<T>) => ReturnType<T> | undefined
  : never

export const lookupValueFromParent = <T>(lookupValue: T): LookupFn<T> | (() => T | undefined) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  type ExtractedFn = T extends (...args: any[]) => any ? T : never

  if (lookupValue == null) {
    return () => undefined
  }

  if (typeof lookupValue === 'function') {
    return lookupValue as ExtractedFn
  }

  if (typeof lookupValue !== 'string') {
    return () => lookupValue as ReturnType<ExtractedFn>
  }

  const targetInstance = inject<ComponentInternalInstance>(INSTANCE_FOR_PARENT_LOOKUP_CONTEXT)

  if (!targetInstance) {
    return () => undefined
  }

  const parentMethod = (targetInstance.parent?.proxy as unknown as Record<string, unknown>)?.[
    lookupValue
  ]

  return typeof parentMethod === 'function'
    ? (parentMethod as ExtractedFn)
    : // TODO: fix unsafe conversion
      () => lookupValue as unknown as ReturnType<ExtractedFn> | undefined
}
