import { props as tableLiteProps } from './BTableLite.props'
import { props as busyProps } from './composables/useBusy'

export const props = {
  ...tableLiteProps,
  ...busyProps,
}
