import { parseSpan } from './utils/parseSpan'

const spanValidator = (value: number | string | null) => {
  if (value === null) {
    return true
  }
  const parsedValue = parseSpan(value)

  return parsedValue == null || parsedValue > 0
}

export const props = {
  colspan: { type: [Number, String], validator: spanValidator },
  rowspan: { type: [Number, String], validator: spanValidator },
  stackedHeading: { type: String },
  stickyColumn: { type: Boolean, default: false },
  variant: { type: String },
}
