import { props as idProps } from '../../composables/useSafeId'
import { props as tableStylingProps } from './composables/useTableSimpleStyling'

export const props = {
  ...idProps,
  ...tableStylingProps,
}
