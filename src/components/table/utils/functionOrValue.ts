export const functionOrValue = <T>(value: T, ...args: unknown[]): T => {
  if (typeof value === 'function') {
    return value(...args)
  } else {
    return value
  }
}
