import { it, describe, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import BTableLite from './BTableLite.vue'
import { wrapWithMethods } from './feature-specs/helpers/wrapWithMethods'

const items1 = [
  { a: 1, b: 2, c: 3 },
  { a: 4, b: 5, c: 6 },
]
const fields1 = ['a', 'b', 'c']

describe('table-lite', () => {
  const getDefaultProps = () => ({
    items: items1,
    fields: fields1,
  })

  it('has expected default classes', async () => {
    const wrapper = mount(BTableLite, { props: { items: [], fields: [] } })
    const table = wrapper.find('table')

    expect(table.exists()).toBe(true)
    expect(table.classes().sort()).toEqual(['b-table', 'table'])
    expect(table.text()).toBe('')
  })

  it.each([
    ['striped', 'table-striped'],
    ['bordered', 'table-bordered'],
    ['borderless', 'table-borderless'],
    ['hover', 'table-hover'],
    ['small', 'table-sm'],
    ['dark', 'table-dark'],
    ['outlined', 'border'],
    ['fixed', 'b-table-fixed'],
  ])('has class %s when %s=true', (prop, cssClass) => {
    const wrapper = mount(BTableLite, {
      props: {
        ...getDefaultProps(),
        [prop]: true,
      },
    })
    const table = wrapper.find('table')

    expect(table.exists()).toBe(true)
    expect(table.classes().sort()).toEqual(['b-table', 'table', cssClass].sort())
  })

  it('has class "table-responsive" when responsive=true', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        responsive: true,
      },
    })
    const wrapperDiv = wrapper.find('div')

    expect(wrapperDiv.classes()).toEqual(['table-responsive'])
    expect(wrapperDiv.find('table').classes().sort()).toEqual(['b-table', 'table'])
  })

  it('has class "table-responsive-md" when responsive=md', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        responsive: 'md',
      },
    })
    const wrapperDiv = wrapper.find('div')
    expect(wrapperDiv.classes()).toEqual(['table-responsive-md'])
    expect(wrapperDiv.find('table').classes().sort()).toEqual(['b-table', 'table'])
  })

  it('has class stacked when b-table-stacked=true', () => {
    const wrapper = mount(BTableLite, {
      props: { stacked: true },
    })
    const table = wrapper.find('table')

    expect(table.exists()).toBe(true)
    expect(table.classes().sort()).toEqual(['b-table', 'b-table-stacked', 'table'].sort())
  })

  it('has class stacked-md when b-table-stacked=md', () => {
    const wrapper = mount(BTableLite, {
      props: { stacked: 'md' },
    })
    const table = wrapper.find('table')

    expect(table.exists()).toBe(true)
    expect(table.classes().sort()).toEqual(['b-table', 'b-table-stacked-md', 'table'].sort())
  })

  it('stacked and responsive work together', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        ...getDefaultProps(),
        stacked: true,
        responsive: true,
      },
    })

    const wrapperDiv = wrapper.find('div')
    expect(wrapperDiv.classes()).toEqual(['table-responsive'])

    const $table = wrapperDiv.find('table')
    expect($table.exists()).toBe(true)
    expect($table.classes().sort()).toEqual(['b-table', 'b-table-stacked', 'table'])
  })

  it('stacked has data-label attribute on all tbody > tr td', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        ...getDefaultProps(),
        stacked: true,
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(2)
    const $trs = wrapper.findAll('tbody > tr')

    // Labels will have run through startCase
    expect($trs[0].findAll('td')[0].attributes('data-label')).toBe('A')
    expect($trs[1].findAll('td')[0].attributes('data-label')).toBe('A')

    expect($trs[0].findAll('td')[1].attributes('data-label')).toBe('B')
    expect($trs[1].findAll('td')[1].attributes('data-label')).toBe('B')

    expect($trs[0].findAll('td')[2].attributes('data-label')).toBe('C')
    expect($trs[1].findAll('td')[2].attributes('data-label')).toBe('C')
  })

  it('item _rowVariant works', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        items: [{ a: 1, _rowVariant: 'primary' }],
        fields: ['a'],
        dark: false,
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.find('tbody > tr').classes()).toContain('table-primary')

    await wrapper.setProps({
      dark: true,
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.find('tbody > tr').classes()).toContain('bg-primary')
  })

  it('item _cellVariants works', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        items: [{ a: 1, _cellVariants: { a: 'info' } }],
        fields: ['a'],
        dark: false,
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(1)
    expect(wrapper.find('tbody > tr > td').classes()).toContain('table-info')

    await wrapper.setProps({
      dark: true,
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(1)
    expect(wrapper.find('tbody > tr > td').classes()).toContain('bg-info')
  })

  it('changing items array works', async () => {
    const items1 = [
      { a: 1, b: 2 },
      { a: 3, b: 4 },
    ]
    const items2 = [{ a: 3, b: 4 }]
    const wrapper = mount(BTableLite, {
      props: {
        items: items1,
        fields: ['a', 'b'],
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(2)
    await wrapper.setProps({
      items: items2,
    })
    expect(wrapper.findAll('tbody > tr').length).toBe(1)
  })

  it('tbody-tr-class works', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        items: [
          { a: 1, b: 2 },
          { a: 3, b: 4 },
        ],
        fields: ['a', 'b'],
        tbodyTrClass: 'foobar',
      },
    })

    // Prop as a string
    expect(wrapper.findAll('tbody > tr').length).toBe(2)
    let $trs = wrapper.findAll('tbody > tr')
    expect($trs[0].classes()).toContain('foobar')
    expect($trs[1].classes()).toContain('foobar')

    // As a function
    await wrapper.setProps({
      tbodyTrClass: (item: any) => {
        return item.a === 1 ? 'foo' : 'bar'
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(2)
    $trs = wrapper.findAll('tbody > tr')
    expect($trs[0].classes()).toContain('foo')
    expect($trs[0].classes()).not.toContain('bar')
    expect($trs[1].classes()).toContain('bar')
    expect($trs[1].classes()).not.toContain('foo')
  })

  it('thead and tfoot variant and classes work', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        items: [{ a: 1, b: 2 }],
        fields: ['a', 'b'],
        footClone: true,
      },
    })

    expect(wrapper.findAll('thead > tr').length).toBe(1)
    expect(wrapper.findAll('tfoot > tr').length).toBe(1)

    expect(wrapper.find('thead').classes().length).toBe(0)
    expect(wrapper.find('tfoot').classes().length).toBe(0)

    await wrapper.setProps({
      headVariant: 'light',
    })

    expect(wrapper.find('thead').classes()).toContain('thead-light')
    expect(wrapper.find('tfoot').classes()).toContain('thead-light')

    await wrapper.setProps({
      footVariant: 'dark',
    })

    expect(wrapper.find('thead').classes()).toContain('thead-light')
    expect(wrapper.find('tfoot').classes()).toContain('thead-dark')

    await wrapper.setProps({
      theadClass: 'foo',
      tfootClass: 'bar',
    })

    expect(wrapper.find('thead').classes()).toContain('thead-light')
    expect(wrapper.find('thead').classes()).toContain('foo')
    expect(wrapper.find('tfoot').classes()).toContain('thead-dark')
    expect(wrapper.find('tfoot').classes()).toContain('bar')

    await wrapper.setProps({
      theadTrClass: 'willy',
      tfootTrClass: 'wonka',
    })

    expect(wrapper.find('thead > tr').classes()).toContain('willy')
    expect(wrapper.find('tfoot > tr').classes()).toContain('wonka')
  })

  it('item field isRowHeader works', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        items: [{ a: 1, b: 2 }],
        fields: [{ key: 'a', isRowHeader: true }, 'b'],
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > *').length).toBe(2)

    expect(wrapper.findAll('tbody > tr > *')[0].element.tagName).toBe('TH')
    expect(wrapper.findAll('tbody > tr > *')[0].attributes('role')).toBe('rowheader')
    expect(wrapper.findAll('tbody > tr > *')[0].attributes('scope')).toBe('row')

    expect(wrapper.findAll('tbody > tr > *')[1].element.tagName).toBe('TD')
    expect(wrapper.findAll('tbody > tr > *')[1].attributes('role')).toBe('cell')
    expect(wrapper.findAll('tbody > tr > *')[1].attributes('scope')).toBeUndefined()
  })

  it('item field tdAttr and tdClass works', async () => {
    const wrapper = mount(
      wrapWithMethods(BTableLite, {
        parentTdAttrs() {
          return { 'data-parent': 'parent' }
        },
      }),
      {
        props: {
          items: [{ a: 1, b: 2, c: 3 }],
          fields: [
            { key: 'a', tdAttr: { 'data-foo': 'bar' } },
            { key: 'b', tdClass: () => 'baz' },
            { key: 'c', tdAttr: 'parentTdAttrs' },
          ],
        },
      }
    )

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(3)

    const $tds = wrapper.findAll('tbody > tr > td')

    expect($tds[0].attributes('data-foo')).toBe('bar')
    expect($tds[0].attributes('data-parent')).toBeUndefined()
    expect($tds[0].classes().length).toBe(0)

    expect($tds[1].classes()).toContain('baz')
    expect($tds[1].attributes('data-foo')).toBeUndefined()
    expect($tds[1].attributes('data-parent')).toBeUndefined()

    expect($tds[2].attributes('data-parent')).toBe('parent')
    expect($tds[2].attributes('data-foo')).toBeUndefined()
    expect($tds[2].classes().length).toBe(0)
  })

  it('item field thAttr works', async () => {
    const wrapper = mount(
      wrapWithMethods(BTableLite, {
        parentThAttrs(_value: any, _key: any, _item: any, type: any) {
          return { 'data-type': type }
        },
      }),
      {
        props: {
          items: [{ a: 1, b: 2, c: 3 }],
          fields: [
            { key: 'a', thAttr: { 'data-foo': 'bar' } },
            { key: 'b', thAttr: 'parentThAttrs', isRowHeader: true },
            {
              key: 'c',
              thAttr: (_v: any, _k: any, _i: any, t: any) => {
                return { 'data-type': t }
              },
            },
          ],
        },
      }
    )

    expect(wrapper.findAll('thead > tr').length).toBe(1)
    expect(wrapper.findAll('thead > tr > th').length).toBe(3)
    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(2)
    expect(wrapper.findAll('tbody > tr > th').length).toBe(1)

    const $headerThs = wrapper.findAll('thead > tr > th')
    expect($headerThs[0].attributes('data-foo')).toBe('bar')
    expect($headerThs[0].attributes('data-type')).toBeUndefined()
    expect($headerThs[0].classes().length).toBe(0)

    expect($headerThs[1].attributes('data-foo')).toBeUndefined()
    expect($headerThs[1].attributes('data-type')).toBe('head')
    expect($headerThs[1].classes().length).toBe(0)

    expect($headerThs[2].attributes('data-foo')).toBeUndefined()
    expect($headerThs[2].attributes('data-type')).toBe('head')
    expect($headerThs[2].classes().length).toBe(0)

    const $bodyThs = wrapper.findAll('tbody > tr > th')

    expect($bodyThs[0].attributes('data-foo')).toBeUndefined()
    expect($bodyThs[0].attributes('data-type')).toBe('row')
    expect($bodyThs[0].classes().length).toBe(0)
  })

  it('item field formatter as function works', async () => {
    const wrapper = mount(BTableLite, {
      props: {
        items: [{ a: 1, b: 2 }],
        fields: [
          {
            key: 'a',
            formatter(_value: any, _key: any, item: any) {
              return item.a + item.b
            },
          },
          'b',
        ],
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(2)
    const $tds = wrapper.findAll('tbody > tr > td')
    expect($tds[0].text()).toBe('3')
    expect($tds[1].text()).toBe('2')
  })

  it('item field formatter as string works', async () => {
    const wrapper = mount(
      wrapWithMethods(BTableLite, {
        formatter(_value: any, _key: any, item: any) {
          return item.a + item.b
        },
      }),
      {
        props: {
          items: [{ a: 1, b: 2 }],
          fields: [{ key: 'a', formatter: 'formatter' }, 'b'],
        },
      }
    )

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(2)
    const $tds = wrapper.findAll('tbody > tr > td')
    expect($tds[0].text()).toBe('3')
    expect($tds[1].text()).toBe('2')
  })
})
