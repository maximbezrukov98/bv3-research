import { describe, it, expect } from 'vitest'
import { h } from 'vue'
import { mount } from '@vue/test-utils'
import BTable from '../BTable.vue'

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]
const testFields = ['a', 'b', 'c']

describe('table > caption', () => {
  it('should not have caption by default', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('caption').exists()).toBe(false)
  })

  it('should render named slot `table-caption`', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'table-caption': 'foobar',
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table > caption').exists()).toBe(true)
    expect(wrapper.find('caption').text()).toBe('foobar')
    expect(wrapper.find('caption').attributes('id')).toBeUndefined()
    expect(wrapper.find('table').classes()).not.toContain('b-table-caption-top')
  })

  it('should render scoped slot `table-caption`', async () => {
    let scope = null
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'table-caption': function (props: unknown) {
          scope = props
          return h('b', 'foobar')
        },
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table > caption').exists()).toBe(true)
    expect(scope).toEqual({}) /* scoped is an empty object for caption */
    expect(wrapper.find('caption').find('b').exists()).toBe(true)
    expect(wrapper.find('caption').text()).toBe('foobar')
  })

  it('should render `caption` when prop caption is set', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        caption: 'foobar',
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table > caption').exists()).toBe(true)
    expect(wrapper.find('caption').text()).toBe('foobar')
    expect(wrapper.find('caption').attributes('id')).toBeUndefined()
    expect(wrapper.find('caption').classes()).not.toContain('b-table-caption-top')
  })

  it('should render `caption` when prop caption-html is set', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        captionHtml: '<b>foobar</b>',
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table > caption').exists()).toBe(true)
    expect(wrapper.find('caption').find('b').exists()).toBe(true)
    expect(wrapper.find('caption').text()).toBe('foobar')
    expect(wrapper.find('caption').attributes('id')).toBeUndefined()
    expect(wrapper.find('caption').classes()).not.toContain('b-table-caption-top')
  })

  it('should render `caption` with table class when prop caption-top is set', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        caption: 'foobar',
        captionTop: true,
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table > caption').exists()).toBe(true)
    expect(wrapper.find('caption').text()).toBe('foobar')
    expect(wrapper.find('caption').attributes('id')).toBeUndefined()
    expect(wrapper.find('table').classes()).toContain('b-table-caption-top')
  })

  it('should render `caption` with id attribute when prop stacked is true', async () => {
    const wrapper = mount(BTable, {
      props: {
        id: 'zzz',
        fields: testFields,
        items: testItems,
        caption: 'foobar',
        stacked: true,
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table').attributes('id')).toBe('zzz')
    expect(wrapper.find('table > caption').exists()).toBe(true)
    expect(wrapper.find('caption').text()).toBe('foobar')
    expect(wrapper.find('caption').attributes('id')).toBeDefined()
    expect(wrapper.find('caption').attributes('id')).toBe('zzz__caption_')
  })

  it('should render `caption` with id attribute when prop stacked is sm', async () => {
    const wrapper = mount(BTable, {
      props: {
        id: 'zzz',
        fields: testFields,
        items: testItems,
        caption: 'foobar',
        stacked: 'sm',
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table').attributes('id')).toBe('zzz')
    expect(wrapper.find('table > caption').exists()).toBe(true)
    expect(wrapper.find('caption').text()).toBe('foobar')
    expect(wrapper.find('caption').attributes('id')).toBeDefined()
    expect(wrapper.find('caption').attributes('id')).toBe('zzz__caption_')
  })
})
