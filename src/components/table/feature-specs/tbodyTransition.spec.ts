import { it, describe, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import BTable from '../BTable.vue'

// Stub `<transition-group>` component
// config.global.stubs['transition-group'] = false
// vtuConfig.global.stubs['transition-group'] = TransitionGroupStub

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]
const testFields = ['a', 'b', 'c']

describe('table > tbody transition', () => {
  it('tbody should not be a transition-group component by default', async () => {
    const wrapper = mount(BTable, {
      attachTo: document.body,
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    expect(wrapper.find('*').element.tagName).toBe('TABLE')
    expect(wrapper.find('tbody').exists()).toBe(true)
    expect(wrapper.find('tbody').element.tagName).toBe('TBODY')
    // `<transition-group>` stub doesn't render itself with the specified tag
    expect(wrapper.findComponent('transition-group').exists()).toBe(false)
  })

  it('tbody should be a transition-group component when tbody-transition-props set', async () => {
    const wrapper = mount(BTable, {
      attachTo: document.body,
      props: {
        fields: testFields,
        items: testItems,
        tbodyTransitionProps: {
          name: 'fade',
        },
      },
    })
    expect(wrapper.find('*').element.tagName).toBe('TABLE')
    expect(wrapper.findComponent({ name: 'transition-group' }).exists()).toBe(true)
    expect(wrapper.find('tbody').exists()).toBe(false)
  })

  it('tbody should be a transition-group component when tbody-transition-handlers set', async () => {
    const wrapper = mount(BTable, {
      attachTo: document.body,
      props: {
        fields: testFields,
        items: testItems,
        tbodyTransitionHandlers: {
          onBeforeEnter: () => ({}),
          onAfterEnter: () => ({}),
          onBeforeLeave: () => ({}),
          onAfterLeave: () => ({}),
        },
      },
    })
    expect(wrapper).toBeDefined()
    expect(wrapper.find('*').element.tagName).toBe('TABLE')
    expect(wrapper.findComponent({ name: 'transition-group' }).exists()).toBe(true)
    expect(wrapper.find('tbody').exists()).toBe(false)
  })
})
