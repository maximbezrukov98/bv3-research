import { describe, it, expect } from 'vitest'
import { h } from 'vue'
import { mount } from '@vue/test-utils'
import { NormalizedField, normalizeFields } from '../utils/normalizeFields'
import BTable from '../BTable.vue'

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]
const testFields = ['a', 'b', 'c']

describe('table > colgroup', () => {
  it('should not have colgroup by default', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('colgroup').exists()).toBe(false)
  })

  it('should render named slot `table-colgroup`', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'table-colgroup': '<col><col><col>',
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('table > colgroup').exists()).toBe(true)
    expect(
      wrapper
        .find('colgroup')
        .findAll('col')
        .every((w) => w.exists())
    ).toBe(true)
    expect(wrapper.find('colgroup').findAll('col').length).toBe(3)
  })

  it('should render scoped slot `table-colgroup`', async () => {
    let fields: NormalizedField[] = []
    let columns
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'table-colgroup': function (scope: { fields: NormalizedField[]; columns: number }) {
          fields = scope.fields
          columns = scope.columns
          return h('col', { span: columns })
        },
      },
    })
    expect(wrapper.find('table').exists()).toBe(true)

    expect(columns).toBe(3)
    expect(fields).toEqual(normalizeFields(testFields, []))
    expect(wrapper.find('table > colgroup').exists()).toBe(true)
    expect(
      wrapper
        .find('colgroup')
        .findAll('col')
        .every((w) => w.exists())
    ).toBe(true)
    expect(wrapper.findAll('col').length).toBe(1)
    expect(wrapper.find('col').attributes('span')).toBe('3')
  })
})
