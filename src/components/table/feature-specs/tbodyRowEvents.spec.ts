import { it, describe, expect } from 'vitest'
import { nextTick } from 'vue'
import { mount } from '@vue/test-utils'
import BTable from '../BTable.vue'
import { findContextBySymbolName } from '../../../../tests/utils/findContextBySymbolName'
import { BusyContext } from '../composables/useBusy'

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]
const testFields = ['a', 'b', 'c']

const noop = () => ({})

describe('table > tbody row events', () => {
  it('should emit row-clicked event when a row is clicked', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowClicked: noop,
      },
    })

    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    await $rows[1].trigger('click')

    expect(wrapper.emitted<unknown[]>('row-clicked')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('row-clicked')![0][0]).toEqual(testItems[1]) // Row item
    expect(wrapper.emitted<unknown[]>('row-clicked')![0][1]).toEqual(1) // Row index
    expect(wrapper.emitted<unknown[]>('row-clicked')![0][2]).toBeInstanceOf(MouseEvent) // Event
  })

  it('should not emit row-clicked event when prop busy is set', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onRowClicked: noop,
      },
    })

    expect(wrapper.find('*').element.tagName).toBe('TABLE')
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-clicked')).toBeUndefined()
    await $rows[1].trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()
  })

  it('should not emit row-clicked event when set to busy internally', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowClicked: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    const setBusy = findContextBySymbolName<BusyContext>(
      wrapper.vm.$,
      'bv-table-busy-context'
    )!.setBusy
    setBusy(true)
    await nextTick()

    await $rows[1].trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()
  })

  it('should emit row-dblclicked event when a row is dblclicked', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowDblClicked: noop,
      },
    })

    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-dblclicked')).toBeUndefined()
    await $rows[1].trigger('dblclick')
    expect(wrapper.emitted<unknown[]>('row-dblclicked')!).toBeDefined()
    expect(wrapper.emitted<unknown[]>('row-dblclicked')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('row-dblclicked')![0][0]).toEqual(testItems[1]) // Row item
    expect(wrapper.emitted<unknown[]>('row-dblclicked')![0][1]).toEqual(1) // Row index
    expect(wrapper.emitted<unknown[]>('row-dblclicked')![0][2]).toBeInstanceOf(MouseEvent) // Event
  })

  it('should not emit row-dblclicked event when a row is dblclicked and table busy', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onRowDblClicked: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-dblclicked')).toBeUndefined()
    await $rows[1].trigger('dblclick')
    expect(wrapper.emitted('row-dblclicked')).toBeUndefined()
  })

  it('should emit row-middle-clicked event when a row is middle clicked', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowMIddleClicked: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-middle-clicked')).toBeUndefined()
    await $rows[1].trigger('auxclick', { which: 2 })
    expect(wrapper.emitted<unknown[]>('row-middle-clicked')!).toBeDefined()
    expect(wrapper.emitted<unknown[]>('row-middle-clicked')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('row-middle-clicked')![0][0]).toEqual(testItems[1]) // Row item
    expect(wrapper.emitted<unknown[]>('row-middle-clicked')![0][1]).toEqual(1) // Row index
    expect(wrapper.emitted<unknown[]>('row-middle-clicked')![0][2]).toBeInstanceOf(Event) // Event
  })

  it('should not emit row-middle-clicked event when a row is middle clicked and table busy', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onRowMiddleClicked: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-middle-clicked')).toBeUndefined()
    await $rows[1].trigger('auxclick', { which: 2 })
    expect(wrapper.emitted('row-middle-clicked')).toBeUndefined()
  })

  it('should emit row-contextmenu event when a row is right clicked', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowContextmenu: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-contextmenu')).toBeUndefined()
    await $rows[1].trigger('contextmenu')
    expect(wrapper.emitted<unknown[]>('row-contextmenu')!).toBeDefined()
    expect(wrapper.emitted<unknown[]>('row-contextmenu')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('row-contextmenu')![0][0]).toEqual(testItems[1]) // Row item
    expect(wrapper.emitted<unknown[]>('row-contextmenu')![0][1]).toEqual(1) // Row index
    expect(wrapper.emitted<unknown[]>('row-contextmenu')![0][2]).toBeInstanceOf(Event) // Event
  })

  it('should not emit row-contextmenu event when a row is right clicked and table busy', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onRowContextmenu: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-contextmenu')).toBeUndefined()
    await $rows[1].trigger('contextmenu')
    expect(wrapper.emitted('row-contextmenu')).toBeUndefined()
  })

  it('should emit row-hovered event when a row is hovered', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowHovered: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-hovered')).toBeUndefined()
    await $rows[1].trigger('mouseenter')
    expect(wrapper.emitted<unknown[]>('row-hovered')!).toBeDefined()
    expect(wrapper.emitted<unknown[]>('row-hovered')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('row-hovered')![0][0]).toEqual(testItems[1]) // Row item
    expect(wrapper.emitted<unknown[]>('row-hovered')![0][1]).toEqual(1) // Row index
    expect(wrapper.emitted<unknown[]>('row-hovered')![0][2]).toBeInstanceOf(MouseEvent) // Event
  })

  it('should not emit row-hovered event when a row is hovered and no listener', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-hovered')).toBeUndefined()
    await $rows[1].trigger('mouseenter')
    expect(wrapper.emitted('row-hovered')).toBeUndefined()
  })

  it('should not emit row-hovered event when a row is hovered and table busy', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onRowHovered: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-hovered')).toBeUndefined()
    await $rows[1].trigger('mouseenter')
    expect(wrapper.emitted('row-hovered')).toBeUndefined()
  })

  it('should emit row-unhovered event when a row is unhovered', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowUnhovered: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-unhovered')).toBeUndefined()
    await $rows[1].trigger('mouseleave')
    expect(wrapper.emitted<unknown[]>('row-unhovered')!).toBeDefined()
    expect(wrapper.emitted<unknown[]>('row-unhovered')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('row-unhovered')![0][0]).toEqual(testItems[1]) // Row item
    expect(wrapper.emitted<unknown[]>('row-unhovered')![0][1]).toEqual(1) // Row index
    expect(wrapper.emitted<unknown[]>('row-unhovered')![0][2]).toBeInstanceOf(MouseEvent) // Event
  })

  it('should not emit row-unhovered event when a row is hovered and no listener', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-unhovered')).toBeUndefined()
    await $rows[1].trigger('mouseleave')
    expect(wrapper.emitted('row-unhovered')).toBeUndefined()
  })

  it('should not emit row-unhovered event when a row is unhovered and table busy', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onRowUnhovered: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-unhovered')).toBeUndefined()
    await $rows[1].trigger('mouseleave')
    expect(wrapper.emitted('row-unhovered')).toBeUndefined()
  })

  it('should emit row-clicked event when a row is focusable and enter pressed', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowClicked: noop,
      },
      attachTo: document.body,
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-clicked')).toBeUndefined()
    ;($rows[1].element as HTMLElement).focus() // Event only works when the TR is focused
    await nextTick()
    await $rows[1].trigger('keydown.enter')
    await nextTick()
    expect(wrapper.emitted<unknown[]>('row-clicked')!).toBeDefined()
    expect(wrapper.emitted<unknown[]>('row-clicked')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('row-clicked')![0][0]).toEqual(testItems[1]) // Row item
    expect(wrapper.emitted<unknown[]>('row-clicked')![0][1]).toEqual(1) // Row index
    // Note: the KeyboardEvent is passed to the row-clicked handler
    expect(wrapper.emitted<unknown[]>('row-clicked')![0][2]).toBeInstanceOf(KeyboardEvent) // Event
  })

  it('should not emit row-clicked event when a row is focusable, enter pressed, and table busy', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onRowClicked: noop,
      },
    })
    expect(wrapper).toBeDefined()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect(wrapper.emitted('row-clicked')).toBeUndefined()
    ;($rows[1].element as HTMLElement).focus() // Event only works when the TR is focused
    await $rows[1].trigger('keydown.enter')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()
  })

  it('should not emit row-clicked event when clicking on a button or other interactive element', async () => {
    const wrapper = mount(BTable, {
      attachTo: document.body,
      props: {
        // Add extra virtual columns
        fields: [...testFields, 'd', 'e', 'f'],
        // We just use a single row for testing
        items: [testItems[0]],
        onRowClicked: noop,
      },
      slots: {
        'cell(a)': '<button id="a">button</button>',
        'cell(b)': '<input id="b">',
        'cell(c)': '<a href="#" id="c">link</a>',
        'cell(d)':
          '<div class="dropdown-menu"><div id="d" class="dropdown-item">dropdown</div></div>',
        'cell(e)': '<label for="e">label</label><input id="e">',
        'cell(f)': '<label class="f-label"><input id="e"></label>',
      },
    })
    expect(wrapper).toBeDefined()
    expect(wrapper.element.tagName).toBe('TABLE')
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(1)
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    const $btn = wrapper.find('button[id="a"]')
    expect($btn.exists()).toBe(true)
    await $btn.trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    const $input = wrapper.find('input[id="b"]')
    expect($input.exists()).toBe(true)
    await $input.trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    const $link = wrapper.find('a[id="c"]')
    expect($link.exists()).toBe(true)
    await $link.trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    const $dd = wrapper.find('div[id="d"]')
    expect($dd.exists()).toBe(true)
    await $dd.trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    const $label = wrapper.find('label[for="e"]')
    expect($label.exists()).toBe(true)
    await $label.trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()

    const $labelf = wrapper.find('label.f-label')
    expect($labelf.exists()).toBe(true)
    await $labelf.trigger('click')
    expect(wrapper.emitted('row-clicked')).toBeUndefined()
  })

  it('keyboard events moves focus to appropriate rows', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onRowClicked: noop,
      },
      attachTo: document.body,
    })

    await nextTick()
    const $rows = wrapper.findAll('tbody > tr')
    expect($rows.length).toBe(3)
    expect($rows.every((w) => w.attributes('tabindex') === '0')).toBe(true)

    expect(document.activeElement).not.toBe($rows[0].element)
    expect(document.activeElement).not.toBe($rows[1].element)
    expect(document.activeElement).not.toBe($rows[2].element)
    ;($rows[0].element as HTMLElement).focus()
    expect(document.activeElement).toBe($rows[0].element)

    await $rows[0].trigger('keydown.end')
    expect(document.activeElement).toBe($rows[2].element)

    await $rows[2].trigger('keydown.home')
    expect(document.activeElement).toBe($rows[0].element)

    await $rows[0].trigger('keydown.down')
    expect(document.activeElement).toBe($rows[1].element)

    await $rows[1].trigger('keydown.up')
    expect(document.activeElement).toBe($rows[0].element)

    await $rows[0].trigger('keydown.down', { shiftKey: true })
    expect(document.activeElement).toBe($rows[2].element)

    await $rows[2].trigger('keydown.up', { shiftKey: true })
    expect(document.activeElement).toBe($rows[0].element)

    // Should only move focus if TR was target
    await $rows[0].find('td').trigger('keydown.down')
    expect(document.activeElement).toBe($rows[0].element)
  })
})
