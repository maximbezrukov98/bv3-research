import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import { wrapWithMethods } from './helpers/wrapWithMethods'

import BTable from '../BTable.vue'

describe('table > field-formatter', () => {
  it('item field formatter as function works', async () => {
    const wrapper = mount(BTable, {
      props: {
        items: [{ a: 1, b: 2 }],
        fields: [
          {
            key: 'a',
            formatter: (_value: unknown, _key: unknown, item: any) => item.a + item.b,
          },
          'b',
        ],
      },
    })

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(2)
    const $tds = wrapper.findAll('tbody > tr > td')
    expect($tds[0].text()).toBe('3')
    expect($tds[1].text()).toBe('2')
  })

  it('item field formatter as string works', async () => {
    const wrapper = mount(
      wrapWithMethods(BTable, {
        formatter: (_value: unknown, _key: unknown, item: any) => item.a + item.b,
      }),
      {
        props: {
          items: [{ a: 1, b: 2 }],
          fields: [{ key: 'a', formatter: 'formatter' }, 'b'],
        },
      }
    )

    expect(wrapper.findAll('tbody > tr').length).toBe(1)
    expect(wrapper.findAll('tbody > tr > td').length).toBe(2)
    const $tds = wrapper.findAll('tbody > tr > td')
    expect($tds[0].text()).toBe('3')
    expect($tds[1].text()).toBe('2')
  })
})
