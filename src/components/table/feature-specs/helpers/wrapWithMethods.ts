import { defineComponent } from 'vue'

export const wrapWithMethods = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Component: any,
  methods: Record<string, (...args: unknown[]) => unknown>
) =>
  defineComponent({
    components: { wrappedComponent: Component },
    methods,
    template: `
    <wrapped-component>
      <template v-for="(_, name) in $slots" :slot="name" slot-scope="slotData"><slot :name="name" v-bind="slotData" /></template>
    </wrapped-component>
  `,
  })
