import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import BTable from '../BTable.vue'
import { nextTick, reactive } from 'vue'

describe('table > row details', () => {
  const getDefaultItems = () =>
    reactive([
      { a: 1, b: 2, c: 3, _showDetails: true },
      { a: 5, b: 5, c: 6 },
      { a: 7, b: 8, c: 9, _showDetails: false },
    ])
  const testFields = ['a', 'b', 'c']

  it('does not show details if slot row-details not defined', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: getDefaultItems(),
      },
    })
    const table = wrapper.find('table')

    expect(table.find('tbody').exists()).toBe(true)
    const $trs = table.findAll('tbody > tr')
    expect($trs.length).toBe(3)
    expect($trs.every(($t) => $t.element.matches('tr.b-table-details') === false)).toBe(true)
  })

  it('expected rows have details showing', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: getDefaultItems(),
      },
      slots: {
        'row-details': '<div>foobar</div>',
      },
    })
    const table = wrapper.find('table')

    expect(table.find('tbody').exists()).toBe(true)
    const $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(4)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[2].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[3].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].text()).toBe('foobar')
  })

  it('prop `details-td-class` works', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: getDefaultItems(),
        detailsTdClass: 'foobar-class',
      },
      slots: {
        'row-details': '<div>foobar</div>',
      },
    })
    const table = wrapper.find('table')

    expect(table.find('tbody').exists()).toBe(true)
    const $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(4)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[0].findAll('td').length).toBe(3)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[1].findAll('td').length).toBe(1)
    expect($trs[1].text()).toBe('foobar')
    const $detailsTd = $trs[1].find('td')
    expect($detailsTd.classes().length).toBe(1)
    expect($detailsTd.classes()).toContain('foobar-class')
    expect($trs[2].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[2].findAll('td').length).toBe(3)
    expect($trs[3].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[3].findAll('td').length).toBe(3)
  })

  it('should show details slot when _showDetails changed', async () => {
    const items = getDefaultItems()

    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items,
      },
      slots: {
        // Named slots get turned into scopedSlots in Vue 2.6.x
        'row-details': '<div>foobar</div>',
      },
    })

    expect(wrapper.find('tbody').exists()).toBe(true)
    expect(wrapper.findAll('tbody > tr').length).toBe(4)

    items[2]._showDetails = true
    await nextTick()

    const $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(5)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[1].text()).toBe('foobar')
    expect($trs[2].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[3].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[4].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[4].text()).toBe('foobar')
  })

  it('should hide details slot when _showDetails changed', async () => {
    const items = getDefaultItems()
    const testFields = ['a', 'b', 'c']
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items,
      },
      slots: {
        'row-details': '<div>foobar</div>',
      },
    })
    let $trs
    expect(wrapper.find('tbody').exists()).toBe(true)
    expect(wrapper.findAll('tbody > tr').length).toBe(4)
    $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(4)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[1].text()).toBe('foobar')
    expect($trs[2].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[3].element.matches('tr.b-table-details')).toBe(false)

    items[0]._showDetails = false
    await nextTick()

    $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(3)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[2].element.matches('tr.b-table-details')).toBe(false)
  })

  it('should have extra spacer tr when details showing and striped=true', async () => {
    const testItems = [
      { a: 1, b: 2, c: 3, _showDetails: true },
      { a: 5, b: 5, c: 6 },
      { a: 7, b: 8, c: 9, _showDetails: false },
    ]
    const testFields = ['a', 'b', 'c']
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        striped: true,
      },
      slots: {
        'row-details': '<div>foobar</div>',
      },
    })

    expect(wrapper.find('tbody').exists()).toBe(true)
    const $trs = wrapper.findAll('tbody > tr')

    expect($trs.length).toBe(5)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[0].element.matches('tr.d-none')).toBe(false)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].element.matches('tr.d-none')).toBe(true)
    expect($trs[2].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[2].element.matches('tr.d-none')).toBe(false)
    expect($trs[2].text()).toBe('foobar')
    expect($trs[3].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[3].element.matches('tr.d-none')).toBe(false)
    expect($trs[4].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[4].element.matches('tr.d-none')).toBe(false)
  })

  it('should show details slot when slot method toggleDetails() called', async () => {
    const items = reactive([{ a: 1, b: 2, c: 3, _showDetails: true }])
    const testFields = ['a', 'b', 'c']
    let scopeDetails: { toggleDetails: () => void } | null = null
    let scopeField: { toggleDetails: () => void } | null = null
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items,
      },
      slots: {
        'row-details': function (scope: { toggleDetails: () => void }) {
          scopeDetails = scope
          return 'foobar'
        },
        'cell(a)': function (scope: { toggleDetails: () => void }) {
          scopeField = scope
          return 'AAA'
        },
      },
    })
    let $trs

    expect(wrapper.find('tbody').exists()).toBe(true)
    expect(wrapper.findAll('tbody > tr').length).toBe(2)

    $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(2)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[1].text()).toBe('foobar')

    // Toggle details via details slot
    expect(scopeDetails).not.toBe(null)
    scopeDetails!.toggleDetails()
    await nextTick()

    expect(wrapper.findAll('tbody > tr').length).toBe(1)

    $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(1)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)

    // Toggle details via field slot
    expect(scopeField).not.toBe(null)
    scopeField!.toggleDetails()
    await nextTick()

    expect(wrapper.findAll('tbody > tr').length).toBe(2)

    $trs = wrapper.findAll('tbody > tr')
    expect($trs.length).toBe(2)
    expect($trs[0].element.matches('tr.b-table-details')).toBe(false)
    expect($trs[1].element.matches('tr.b-table-details')).toBe(true)
    expect($trs[1].text()).toBe('foobar')
  })
})
