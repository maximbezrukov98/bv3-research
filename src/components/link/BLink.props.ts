import { PropType } from 'vue'

// We need to keep this separate because it is reused in BButton
export const basicLinkProps = {
  // RouterLink props
  activeClass: { type: String },
  append: { type: Boolean, default: false },
  event: { type: [Array, String] },
  exact: { type: Boolean, default: false },
  exactActiveClass: { type: String },
  exactPath: { type: Boolean, default: false },
  replace: { type: Boolean, default: false },
  routerTag: { type: String },
  to: {
    type: [String, Object] as PropType<
      string | { path?: string; query?: Record<string, string>; hash?: string }
    >,
  },

  // NuxtLink props
  noPrefetch: { type: Boolean, default: false },
  prefetch: { type: Boolean as PropType<boolean | null>, default: null },

  active: { type: Boolean, default: false },
  disabled: { type: Boolean, default: false },
  href: { type: String },
  rel: { type: String as PropType<string | null>, default: null },
  routerComponentName: { type: String },
  target: { type: String, default: '_self' },
}

export const linkProps = {
  ...basicLinkProps,
  // RouterLink props
  event: { type: [Array, String] },
  routerTag: { type: String },
}
